#!/bin/sh

# Check if WordPress is already installed
if [ ! -f /var/www/wordpress/wp-config.php ]; then
	#Download wordpress
	wget https://wordpress.org/latest.tar.gz
	tar -xzvf latest.tar.gz
	rm -rf latest.tar.gz

	cd /var/www/wordpress
    cp wp-config-sample.php wp-config.php

	sed -i "s/username_here/$SQL_USER/g" wp-config.php
	sed -i "s/password_here/$SQL_PASSWORD/g" wp-config.php
	sed -i "s/localhost/$SQL_HOSTNAME/g" wp-config.php
	sed -i "s/database_name_here/$SQL_DATABASE/g" wp-config.php

	#download and install WP-CLI
    wget https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar;
	chmod +x wp-cli.phar; 
	mv wp-cli.phar /usr/local/bin/wp;

	wp core install \
        --allow-root \
        --url=${WP_URL} \
        --title=${WP_TITLE} \
        --admin_user=${WP_ADMIN_NAME} \
        --admin_password=${WP_ADMIN_PASSWORD} \
        --admin_email=${WP_ADMIN_EMAIL}

	wp user create \
        --allow-root ${WP_NAME} ${WP_EMAIL} \
        --user_pass=${WP_PASSWORD};

	

fi

exec "$@"